﻿using db = ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using System.Linq;
using System;

namespace ClientSupportSystem.Model.Helper
{
	public static class Extentions
	{
		public static db.Ticket Convert(this TicketItemModel source)
		{
			var target = new db.Ticket();
			target.Id = source.Id;
			target.Title = source.Title;
			target.Info = source.Info;
			target.StaffId = source.StaffId;
			target.UserId = source.UserId;
			target.DateEnd = source.DateEnd;
			target.DateStart = source.DateStart;
			target.CreateDate = source.CreateDate;
			target.ActionTypeId = source.ActionTypeId;
			target.GroupId = source.GroupId;
			return target;
		}

		public static TicketItemModel Convert(this db.Ticket source)
		{
			var target = new TicketItemModel();
			target.Id = source.Id;
			target.Title = source.Title;
			target.Info = source.Info;
			target.StaffId = source.StaffId;
			target.ActionTypeId = source.ActionTypeId;
			target.ActionTypeName = source.ActionType?.Name;
			target.StaffName = source.Staff?.Name;
			target.UserId = source.UserId;
			target.UserName = source.User?.Name;
			target.DateEnd = source.DateEnd;
			target.DateStart = source.DateStart;
			target.CreateDate = source.CreateDate;
			target.Histories = source.History?.Select(q => q.Convert()).ToList();
			target.GroupId = source.GroupId;
			target.GroupName = source.Group?.Name;
			return target;
		}

		public static HistoryItemModel Convert(this db.History source)
		{
			var history = new HistoryItemModel();
			history.Id = source.Id;
			history.Info = source.Info;
			history.StaffId = source.StaffId;
			history.StaffName = source.User?.Name;
			history.TicketId = source.TicketId;
			history.Date = source.Date;
			return history;
		}

		public static HelpModel Convert(this db.Help source)
		{
			var help = new HelpModel();
			help.Id = source.Id;
			help.Name = source.Name;
			help.Info = source.Info;
			return help;
		}	  
		
		public static UserModel Convert(this db.User source)
		{
			var user = new UserModel();
			user.Id = source.Id;
			user.Name = source.Name;
			user.RoleId = source.RoleId;
			user.RoleName = source.Role?.Name;
			return user;
		}

		public static ActionTypeModel Convert(this db.ActionType source)
		{
			var model = new ActionTypeModel();
			model.Id = source.Id;
			model.Name = source.Name;
			return model;
		}	
		
		public static UserRoleModel Convert(this db.UserRole source)
		{
			var model = new UserRoleModel();
			model.Id = source.Id;
			model.Name = source.Name;
			return model;
		} 
		
		public static GroupItemModel Convert(this db.Group source)
		{
			var model = new GroupItemModel();
			model.Id = source.Id;
			model.Name = source.Name;
			model.Keys = source.KeyWords;
			return model;
		}
	}
}
