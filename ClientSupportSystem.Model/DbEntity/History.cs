﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ClientSupportSystem.Model.DbEntity
{
	public class History : Entity
	{
		public DateTime Date { get; set; }
		public int TicketId { get; set; }
		public int StaffId { get; set; }
		public string Info { get; set; }
		public Ticket Ticket { get; set; }
		[ForeignKey("StaffId")]
		public User User { get; set; }
	}
}
