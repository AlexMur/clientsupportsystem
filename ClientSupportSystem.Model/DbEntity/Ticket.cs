﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ClientSupportSystem.Model.DbEntity
{
	public class Ticket : Entity
	{
		public string Title { get; set; }
		public string Info { get; set; }
		public int? StaffId { get; set; }
		public int UserId { get; set; }
		public DateTime? DateStart { get; set; }
		public DateTime? DateEnd { get; set; }
		public DateTime CreateDate { get; set; }
		public int ActionTypeId { get; set; }
		public ActionType ActionType { get; set; }
		public int? GroupId { get; set; }
		public Group Group{ get; set; }
		[ForeignKey("UserId")]
		public User User { get; set; }
		[ForeignKey("StaffId")]
		public User Staff { get; set; }
		public ICollection<History> History { get; set; }
	}
}
