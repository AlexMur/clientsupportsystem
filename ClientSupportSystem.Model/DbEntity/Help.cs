﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.DbEntity
{
	public class Help : Entity
	{
		public string Name { get; set; }
		public string Info { get; set; }
	}
}
