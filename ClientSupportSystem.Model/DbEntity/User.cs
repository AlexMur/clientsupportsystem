﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ClientSupportSystem.Model.DbEntity
{
	public class User :Entity
	{
		public string Name { get; set; }
		public int RoleId { get; set; }
		[ForeignKey("RoleId")]
		public UserRole Role { get; set; }
	}
}
