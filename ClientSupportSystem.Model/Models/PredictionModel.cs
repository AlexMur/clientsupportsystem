﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class PredictionModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int PredictionScore { get; set; }
	}
}
