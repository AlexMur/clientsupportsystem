﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class HelpModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Info { get; set; }
	}
}
