﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class ActionTypeModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
