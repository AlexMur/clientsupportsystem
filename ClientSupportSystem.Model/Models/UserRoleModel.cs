﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class UserRoleModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
