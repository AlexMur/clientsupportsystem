﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class HistoryItemModel : EntityModel
	{
		public DateTime Date { get; set; }
		public int TicketId { get; set; }
		public int StaffId { get; set; }
		public string StaffName { get; set; }
		public string Info { get; set; }
	}
}
