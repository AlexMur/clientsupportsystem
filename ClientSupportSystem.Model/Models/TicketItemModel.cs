﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class TicketItemModel : EntityModel
	{
		public string Title { get; set; }
		public string Info { get; set; }
		public int? StaffId { get; set; }
		public string StaffName { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public DateTime? DateStart { get; set; }
		public DateTime? DateEnd { get; set; }
		public DateTime CreateDate { get; set; }
		public int ActionTypeId { get; set; }
		public string ActionTypeName { get; set; }
		public IEnumerable<HistoryItemModel> Histories { get; set; }
		public int? GroupId { get; set; }
		public string GroupName { get; set; }
	}
}
