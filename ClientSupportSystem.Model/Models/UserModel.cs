﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class UserModel : EntityModel
	{
		public string Name { get; set; }
		public int RoleId { get; set; }
		public string RoleName { get; set; }
	}
}
