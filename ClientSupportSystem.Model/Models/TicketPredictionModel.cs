﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class TicketPredictionModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Info { get; set; }
	}
}
