﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public enum ActionTypes
	{
		New = 1,
		InWork,
		Done,
		Canceled,
		Stopped
	}
}
