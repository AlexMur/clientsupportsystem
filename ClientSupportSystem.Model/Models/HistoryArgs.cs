﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Model.Models
{
	public class HistoryArgs
	{
		public int Id { get; set; }
		public string Info { get; set; }
		public int TicketId { get; set; }
		public int StaffId { get; set; }
	}
}
