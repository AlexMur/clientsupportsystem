﻿using ClientSupportSystem.Model.DbEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.DB
{
	public class ClientSupportContext1 : DbContext
	{
		public ClientSupportContext1(DbContextOptions options) : base(options) { }

		public DbSet<Ticket> Tickets { get; set; }
		public DbSet<History> Histories { get; set; }
		public DbSet<ActionType> ActionTypes { get; set; }
		public DbSet<Help> Helps { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserRole> UserRoles { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//modelBuilder.Entity<Ticket>().HasMany(q => q.History).WithOne(q => q.Ticket).HasForeignKey(q=>q.TicketId);
			//modelBuilder.Entity<History>().HasOne(q => q.ActionType).WithMany(q => q.Histories).HasForeignKey(q => q.ActionTypeId);
		}
	}
}
