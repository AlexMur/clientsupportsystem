﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public class HistoryRepository : IHistoryRepository
	{
		private ClientSupportContext _db;

		public HistoryRepository(ClientSupportContext db)
		{
			_db = db;
		}

		public int AddHistory(HistoryItemModel item)
		{
			_db.Add(new History
			{
				Date = DateTime.Now,
				Info = item.Info,
				StaffId = item.StaffId,
				TicketId = item.TicketId,
				//ActionTypeId = //CREATE
			});
			_db.SaveChanges();
			return item.Id;
		}

		public void DeleteHistory(int id)
		{
			var item = _db.Histories.FirstOrDefault(q => q.Id == id);
			_db.Remove(item);
			_db.SaveChanges();
		}

		public void UpdateHistory(HistoryItemModel item)
		{
			var history = _db.Histories.FirstOrDefault(q => q.Id == item.Id);
			history.Date = DateTime.Now;
			history.Info = item.Info;
			history.StaffId = item.StaffId;
			//ActionTypeId = //UPDATE
			_db.Update(history);
			_db.SaveChanges();
		}
	}
}
