﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Helper;
using ClientSupportSystem.Model.Models;

namespace ClientSupportSystem.Repository
{
	public class RoleRepository : IRoleRepository
	{
		private ClientSupportContext _role;

		public RoleRepository(ClientSupportContext role)
		{
			_role = role;
		}

		public IEnumerable<UserRoleModel> GetAll()
		{
			return _role.UserRoles.Select(q => q.Convert()).ToList();
		}
	}
}
