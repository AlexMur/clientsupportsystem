﻿using ClientSupportSystem.Model.Helper;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using db = ClientSupportSystem.Model.DbEntity;


namespace ClientSupportSystem.Repository
{
	public class HelpRepository : IHelpRepostory
	{
		private ClientSupportContext _helpRepo;

		public HelpRepository(ClientSupportContext helpRepo)
		{
			_helpRepo = helpRepo;
		}

		public void Create(HelpModel model)
		{
            var item = new db.Help();
            item.Info = model.Info;
            item.Name = model.Name;
            _helpRepo.Add(item);
			_helpRepo.SaveChanges();
		}

		public IEnumerable<HelpModel> GetAll()
		{
			return _helpRepo.Helps.Select(q => q.Convert()).ToList();
		}

		public HelpModel GetById(int id)
		{
			return _helpRepo.Helps.FirstOrDefault(q => q.Id == id)?.Convert();
		}

		public void Remove(int id)
		{
			var item = _helpRepo.Helps.FirstOrDefault(q => q.Id == id);
			_helpRepo.Helps.Remove(item);
			_helpRepo.SaveChanges();
		}

		public void Update(db.Help item)
		{
			var old = _helpRepo.Helps.FirstOrDefault(q => q.Id == item.Id);
			old.Name = item.Name;
			old.Info = item.Info;
			_helpRepo.Update(old);
			_helpRepo.SaveChanges();
		}
	}
}
