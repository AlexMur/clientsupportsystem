﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Helper;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public class ActionRepository : IActionRepository
	{
		private ClientSupportContext _repo;

		public ActionRepository(ClientSupportContext repo)
		{
			_repo = repo;
		}

		public IEnumerable<ActionTypeModel> GetAll()
		{
			return _repo.ActionTypes.Select(q => q.Convert()).ToList();
		}
	}
}
