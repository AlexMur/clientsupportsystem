﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Model.Helper;
using db = ClientSupportSystem.Model.DbEntity;
using Microsoft.EntityFrameworkCore;
using ClientSupportSystem.Model.DbEntity;

namespace ClientSupportSystem.Repository
{
	public class TicketRepository : ITicketRepository
	{
		private ClientSupportContext _dbContext;

		public TicketRepository(ClientSupportContext context)
		{
			_dbContext = context;
		}

		public int CreateTicket(TicketItemModel item)
		{
			item.CreateDate = DateTime.Now;
			item.ActionTypeId = item.ActionTypeId;// (int)ActionTypes.New;
			var ticket = item.Convert();
			_dbContext.Tickets.Add(ticket);
			_dbContext.SaveChanges();
			return ticket.Id;
		}

		public void DeleteTicket(int id)
		{
			var item = _dbContext.Tickets.Include(nameof(History)).FirstOrDefault(q => q.Id == id);
			_dbContext.Tickets.Remove(item);
			_dbContext.SaveChanges();
		}

		public IEnumerable<TicketPredictionModel> GetAllPredictionTickets(int? max)
		{
			var data = _dbContext.Tickets.Where(q => q.ActionTypeId == (int)ActionTypes.Done);
			if (max.HasValue)
			{
				data = data.Take(max.Value);
			}
			return data.Select(q => new TicketPredictionModel { Id = q.Id, Title = q.Title, Info = q.Info }).ToList();
		}

		public IEnumerable<TicketItemModel> GetAllTickets()
		{
			var items = _dbContext.Tickets
					.Include(nameof(User))
					.Include(nameof(ActionType))
					.Include("Staff")
					.Include(nameof(Group))
					.ToList();
			return items.Select(q => q.Convert()).ToList();
		}

		public TicketItemModel GetTicket(int id)
		{
			var item = _dbContext.Tickets
				.Include(nameof(History))
				.Include(nameof(User))
				.Include(nameof(ActionType))
				.FirstOrDefault(q => q.Id == id);
			return item?.Convert();
		}

		public void UpdateTicket(TicketItemModel item)
		{
			var ticket = _dbContext.Tickets.FirstOrDefault(q => q.Id == item.Id);
			ticket.Info = item.Info;
			ticket.StaffId = item.StaffId;
			ticket.Title = item.Title;
			ticket.UserId = item.UserId;
			ticket.DateEnd = item.DateEnd;
			ticket.DateStart = item.DateStart;
			ticket.ActionTypeId = item.ActionTypeId;
			ticket.GroupId = item.GroupId;
			_dbContext.Tickets.Update(ticket);
			_dbContext.SaveChanges();
		}
	}
}
