﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Helper;
using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientSupportSystem.Repository.Repositories
{
	public class GroupsRepository : IGroupsRepository
	{
		private ClientSupportContext _repo;

		public GroupsRepository(ClientSupportContext repo)
		{
			_repo = repo;
		}

		public int Create(GroupItemModel item)
		{
			var gr = new Group();
			gr.Name = item.Name;
			gr.KeyWords = item.Keys;

			_repo.Groups.Add(gr);
			_repo.SaveChanges();

			return gr.Id;
		}

		public void Delete(int id)
		{
			var gr = _repo.Groups.FirstOrDefault(q => q.Id == id);

			_repo.Groups.Remove(gr);

			_repo.SaveChanges();
		}

		public IEnumerable<GroupItemModel> GetAll()
		{
			return _repo.Groups.ToList().Select(q => q.Convert());
		}

		public GroupItemModel GetById(int id)
		{
			var gr = _repo.Groups.FirstOrDefault(q => q.Id == id);

			return gr.Convert();
		}

		public void Update(GroupItemModel item)
		{
			var gr = _repo.Groups.FirstOrDefault(q => q.Id == item.Id);
			gr.Name = item.Name;
			gr.KeyWords = item.Keys;

			_repo.Groups.Update(gr);

			_repo.SaveChanges();
		}
	}
}
