﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Helper;
using ClientSupportSystem.Model.Models;
using Microsoft.EntityFrameworkCore;

namespace ClientSupportSystem.Repository
{
	public class UsersRepository : IUsersRepository
	{
		private ClientSupportContext _userRepo;

		public UsersRepository(ClientSupportContext repo)
		{
			_userRepo = repo;
		}

		public UserModel GetAdmin()
		{
			return _userRepo.Users.FirstOrDefault(q => q.Name.Contains("администратор"))?.Convert();
		}

		public IEnumerable<UserModel> GetAll()
		{
			return _userRepo.Users
			.Include("Role")
			.Select(q => q.Convert()).ToList();
		}

		public IEnumerable<UserModel> GetStaff()
		{
			return _userRepo.Users.Where(q => q.RoleId != (int)Roles.User).ToList().Select(q => q.Convert());
		}

		public UserModel GetUser(int id)
		{
			return _userRepo.Users.FirstOrDefault(q => q.Id == id)?.Convert();
		}

		public IEnumerable<UserModel> GetUsers()
		{
			return _userRepo.Users.Where(q => q.RoleId == (int)Roles.User).ToList().Select(q => q.Convert());
		}

		public void SaveUser(UserModel model)
		{
			_userRepo.Add(new User { Name = model.Name, RoleId = model.RoleId });
			_userRepo.SaveChanges();
		}

		public void UpdateUser(UserModel model)
		{
			var item = _userRepo.Users.FirstOrDefault(q => q.Id == model.Id);
			item.Name = model.Name;
			item.RoleId = model.RoleId;
			_userRepo.SaveChanges();
		}
	}
}
