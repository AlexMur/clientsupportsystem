﻿using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface IUsersRepository
	{
		IEnumerable<UserModel> GetUsers();
		IEnumerable<UserModel> GetStaff();
		IEnumerable<UserModel> GetAll();
		UserModel GetUser(int id);
		void UpdateUser(UserModel model);
		void SaveUser(UserModel model);
		UserModel GetAdmin();
	}
}
