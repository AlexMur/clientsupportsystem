﻿using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface IHistoryRepository
	{
		int AddHistory(HistoryItemModel item);
		void UpdateHistory(HistoryItemModel item);
		void DeleteHistory(int id);
	}
}
