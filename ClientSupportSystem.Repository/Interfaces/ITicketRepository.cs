﻿using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface ITicketRepository
	{
		IEnumerable<TicketItemModel> GetAllTickets();
		TicketItemModel GetTicket(int id);
		int CreateTicket(TicketItemModel item);
		void UpdateTicket(TicketItemModel item);
		void DeleteTicket(int id);
		IEnumerable<TicketPredictionModel> GetAllPredictionTickets(int? max);
	}
}
