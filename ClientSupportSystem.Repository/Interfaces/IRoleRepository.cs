﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface IRoleRepository
	{
		IEnumerable<UserRoleModel> GetAll();
	}
}
