﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface IActionRepository
	{
		IEnumerable<ActionTypeModel> GetAll();
	}
}
