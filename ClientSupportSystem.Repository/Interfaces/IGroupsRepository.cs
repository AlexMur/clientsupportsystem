﻿using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository.Interfaces
{
	public interface IGroupsRepository
	{
		IEnumerable<GroupItemModel> GetAll();
		GroupItemModel GetById(int id);
		int Create(GroupItemModel item);
		void Update(GroupItemModel item);
		void Delete(int id);
	}
}
