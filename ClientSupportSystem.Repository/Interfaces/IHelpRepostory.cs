﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.Repository
{
	public interface IHelpRepostory
	{
		IEnumerable<HelpModel> GetAll();
		HelpModel GetById(int id);
		void Create(HelpModel model);
		void Update(Help item);
		void Remove(int id);
	}
}
