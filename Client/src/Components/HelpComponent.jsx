import React from 'react';
import { getHelp, saveHelp, updateHelp, deleteHelp } from '../ApiService';

class HelpComponent extends React.Component {
	constructor(props) {
		super(props)
		let id = this.props.match.params.id;
		this.state = {
			id: id,
			help: {
				name: '',
				info: ''
			}
		}
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.delete = this.delete.bind(this);
	}

	async componentDidMount() {
		if(this.state.id){
			let help = await getHelp(this.state.id);
			this.setState({ help });
		}
	}

	async delete() {
		await deleteHelp(this.state.id);
		this.props.history.push('/helps')
	}

	cancel(event){
		event.stopPropagation();
        event.preventDefault();
		this.props.history.push('/helps')
	}

	async save(event){
		event.stopPropagation();
        event.preventDefault();

		let res;
		if(this.state.id){
			res = await updateHelp(this.state.help);
		} else {
			res = await saveHelp(this.state.help);
		}
        if(res.ok){
            this.props.history.push('/helps');
        }
	}

	render() {
		return <div className="table">
			<div>
				<a href="#" onClick={this.save}>Сохранить</a>
				<a href="#" onClick={this.cancel}>Отмена</a>
				{this.state.id && <a href="#" onClick={this.delete}>Удалить</a>}
			</div>
			<div>
				<div className="left-column">
					<label>Название:</label>
				</div>
				<div className="right-column">
					<input value={this.state.help.name} onChange={(event)=>{
							let help = this.state.help; 
							help.name = event.target.value;
							this.setState({ help })
					}}></input>
				</div>
			</div>
			<div>
				<div className="left-column">
					<label>Содержимое:</label>
				</div>
				<div className="right-column">
					<textarea value={this.state.help.info} onChange={(event)=>{
							let help = this.state.help; 
							help.info = event.target.value;
							this.setState({ help })
					}}></textarea>
				</div>
			</div>
		</div>
	};
}

export default HelpComponent;