import React from 'react';
import { getAllPersonal } from '../ApiService';
import { Link } from "react-router-dom";

class UsersComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: []
		}

		this.getShortName = this.getShortName.bind(this);
	}

	async componentDidMount() {
		let data = await getAllPersonal();
		this.setState({ data });
	}

	getShortName(name){
        var words = name.split(" ");
        var result = words[0];
        for(var i = 1; i < words.length; i ++){
            result += " " + words[i][0] + ".";
        }
        return result;
    }

	render() {
		const rows = this.state.data.map((q,i)=>
			<tr key={i}>
				<td>{this.getShortName(q.name)}</td>
				<td>{q.roleName}</td>
				<td>
					<Link to={'/user/' + q.id}>Редактировать</Link>
				</td>
			</tr>
		);

		return <div className="main-content">
			<div>
				<Link to={'/user/'}>Добавить</Link>
			</div>
			<table>
				<thead>
					<tr>
						<th>Пользователь</th>
						<th>Уровень</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</table>
		</div>
	};
}

export default UsersComponent;