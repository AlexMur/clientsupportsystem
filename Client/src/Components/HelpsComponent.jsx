import React from 'react';
import { getAllHelps } from '../ApiService';
import { Link } from "react-router-dom";

class HelpsComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: []
		}
	}

	async componentDidMount() {
		let data = await getAllHelps();
		this.setState({ data });
	}

	render() {
		const rows = this.state.data.map((q,i)=>
			<tr key={i}>
				<td>{q.name}</td>
				<td>
					<Link to={'/help/' + q.id}>Редактировать</Link>
				</td>
			</tr>
		);

		return <div className="main-content">
			<div>
				<Link to={'/help/'}>Добавить</Link>
			</div>
			<table>
				<thead>
					<tr>
						<th>Название</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</table>
		</div>
	};
}

export default HelpsComponent;