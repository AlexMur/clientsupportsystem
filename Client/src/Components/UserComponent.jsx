import React from 'react';
import { getUser, getRoles, saveUser, updateUser } from '../ApiService';

class UserComponent extends React.Component {
	constructor(props) {
		super(props)
		let id = this.props.match.params.id;
		this.state = {
			id: id,
			user: {
				name: '',
				roleId: 1
			},
			levels: []
		}
		this.saveUser = this.saveUser.bind(this);
		this.cancel = this.cancel.bind(this);
	}

	async componentDidMount() {
		var levels = await getRoles();
		if(this.state.id){
			let user = await getUser(this.state.id);
			this.setState({ user });
		}
		this.setState({ levels });
	}

	cancel(event){
		event.stopPropagation();
        event.preventDefault();
		this.props.history.push('/users')
	}

	async saveUser(event){
		event.stopPropagation();
        event.preventDefault();

		let res;
		if(this.state.id){
			res = await updateUser(this.state.user);
		} else {
			res = await saveUser(this.state.user);
		}
        if(res.ok){
            this.props.history.push('/users');
        }
	}

	render() {
		const rows = this.state.levels.map((q, i) =>
			<option key={i} value={q.id}>{q.name}</option>
		);

		return <div className="table">
			<div>
				<a href="#" onClick={this.saveUser}>Сохранить</a>
				<a href="#" onClick={this.cancel}>Отмена</a>
			</div>
			<div>
				<div className="left-column">
                    <label>Фио:</label>
                </div>
				<div className="right-column">
					<input value={this.state.user.name} onChange={(event)=>{
							let user = this.state.user; 
							user.name = event.target.value;
							this.setState({ user })
					}}></input>
				</div>
			</div>
			<div>
				<div className="left-column">
                    <label>Уровень:</label>
                </div>
				<div className="right-column">
					<select value={this.state.user.roleId} onChange={(event)=>{
						let user = this.state.user; 
						user.roleId = event.target.value;
						this.setState({ user })
					}}>
						{rows}
					</select>
				</div>
			</div>
		</div>
	};
}

export default UserComponent;