import React from 'react';
import { getGroup, saveGroup, updateGroup, deleteGroup } from '../ApiService';

class GroupComponent extends React.Component {
	constructor(props) {
		super(props)
		let id = this.props.match.params.id;
		this.state = {
			id: id,
			group: {
				name: '',
				keys: ''
			}
		}
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.delete = this.delete.bind(this);
	}

	async componentDidMount() {
		if(this.state.id){
			let group = await getGroup(this.state.id);
			this.setState({ group });
		}
	}

	async delete() {
		await deleteGroup(this.state.id);
		this.props.history.push('/groups')
	}

	cancel(event){
		event.stopPropagation();
        event.preventDefault();
		this.props.history.push('/groups')
	}

	async save(event){
		event.stopPropagation();
        event.preventDefault();

		let res;
		if(this.state.id){
			res = await updateGroup(this.state.group);
		} else {
			res = await saveGroup(this.state.group);
		}
        if(res.ok){
            this.props.history.push('/groups');
        }
	}

	render() {
		return <div className="table">
			<div>
				<a href="#" onClick={this.save}>Сохранить</a>
				<a href="#" onClick={this.cancel}>Отмена</a>
				{this.state.id && <a href="#" onClick={this.delete}>Удалить</a>}
			</div>
			<div>
				<div className="left-column">
					<label>Название:</label>
				</div>
				<div className="right-column">
					<input value={this.state.group.name} onChange={(event)=>{
							let group = this.state.group; 
							group.name = event.target.value;
							this.setState({ group })
					}}></input>
				</div>
			</div>
			<div>
				<div className="left-column">
					<label>Ключ. слова:</label>
				</div>
				<div className="right-column">
					<input value={this.state.group.keys} onChange={(event)=>{
							let group = this.state.group; 
							group.keys = event.target.value;
							this.setState({ group })
					}}></input>
				</div>
			</div>
		</div>
	};
}

export default GroupComponent;