import React from 'react';
import { getHelp } from '../ApiService';

class HelpViewComponent extends React.Component {
	constructor(props) {
		super(props)
		let id = this.props.match.params.id;
		this.state = {
            id: id,
			help: {}
		}
    }

    async componentDidMount() {
        console.log(this.state.id)
        let help = await getHelp(this.state.id);
        this.setState({ help });
	}

    render() {
		return <div>
			<div>
				{this.state.help.name} 
			</div>
			<div>
				{this.state.help.info}
			</div>
		</div>
	};
}

export default HelpViewComponent;