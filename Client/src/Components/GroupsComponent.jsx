import React from 'react';
import { getAllGroups } from '../ApiService';
import { Link } from "react-router-dom";

class GroupsComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: []
		}
	}

	async componentDidMount() {
		let data = await getAllGroups();
		this.setState({ data });
	}

	render() {
		const rows = this.state.data.map((q,i)=>
			<tr key={i}>
				<td>{q.name}</td>
				<td>
					<Link to={'/group/' + q.id}>Редактировать</Link>
				</td>
			</tr>
		);

		return <div className="main-content">
			<div>
				<Link to={'/group/'}>Добавить</Link>
			</div>
			<table>
				<thead>
					<tr>
						<th>Название</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</table>
		</div>
	};
}

export default GroupsComponent;