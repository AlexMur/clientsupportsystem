const url = 'http://localhost:49372/';

  export async function getAllTickets() {
        return await fetch(url + 'tickets', {
                method: 'GET',
                credentials: 'include'
              }).then(response => {
                if (response.status === 200) {
                  return response.json();
                } else {
                  throw new Error('Something went wrong on api server!');
                }
              }).then(response => {
                return response;
              });
    }

    export async function getTicket(id) {
      return await fetch(url + 'tickets/Get' + '?id=' + id, {
              method: 'GET',
              credentials: 'include'
            }).then(response => {
              if (response.status === 200) {
                return response.json();
              } else {
                throw new Error('Something went wrong on api server!');
              }
            }).then(response => {
              return response;
            });
  }

  export async function getUsers() {
    return await fetch(url + 'users/GetUsers', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            } else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getUser(id) {
    return await fetch(url + 'users/GetUser?id=' + id, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            } else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getStaff() {
    return await fetch(url + 'users/GetStaff', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            } else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export function saveTicket(data) {
    return fetch(url + 'tickets/Create ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function updateTicket(data) {
    return fetch(url + 'tickets/Update ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function addHistory(data) {
    return fetch(url + 'tickets/AddHistory ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function deleteHistory(id) {
    return fetch(url + 'tickets/RemoveHistory ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(id)
          }).then(response => {
              return response;
          });
  }

  export function deleteTicket(id) {
    return fetch(url + 'tickets/Delete ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(id)
          }).then(response => {
              return response;
          });
  }

  export function deleteHelp(id) {
    return fetch(url + 'helps/Delete ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(id)
          }).then(response => {
              return response;
          });
  }

  export function updateHistory(data) {
    return fetch(url + 'tickets/UpdateHistory ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export async function getHelpPrediction(text) {
    return await fetch(url + 'Prediction/GetHelpPrediction?text=' + text, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else if(response.status === 204){
              return {};
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getGroupPrediction(text) {
    return await fetch(url + 'Prediction/GetGroupPrediction?text=' + text, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else if(response.status === 204){
              return {};
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getTaskPrediction(text, id) {
    return await fetch(url + 'Prediction/GetTasksPrediction?text=' + text + '&id=' + id, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else if(response.status === 204){
              return [];
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getActions() {
    return await fetch(url + 'Action/GetActions', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getRoles() {
    return await fetch(url + 'Role/GetRoles', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getAllPersonal() {
    return await fetch(url + 'Users/getAllPersonal', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export function updateUser(data) {
    return fetch(url + 'Users/UpdateUser ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function saveUser(data) {
    return fetch(url + 'Users/CreateUser ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export async function getAllHelps() {
    return await fetch(url + 'Helps/GetAll', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getAllGroups() {
    return await fetch(url + 'Groups/GetAll', {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export async function getHelp(id) {
    return await fetch(url + 'Helps/GetById?id=' + id, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export function saveHelp(data) {
    return fetch(url + 'Helps/Create ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function updateHelp(data) {
    return fetch(url + 'Helps/Update ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export async function getGroup(id) {
    return await fetch(url + 'Groups/GetById?id=' + id, {
            method: 'GET',
            credentials: 'include'
          }).then(response => {
            if (response.status === 200) {
              return response.json();
            }
            else {
              throw new Error('Something went wrong on api server!');
            }
          }).then(response => {
            return response;
          });
  }

  export function saveGroup(data) {
    return fetch(url + 'Groups/Create ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function updateGroup(data) {
    return fetch(url + 'Groups/Update ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
          }).then(response => {
              return response;
          });
  }

  export function deleteGroup(id) {
    return fetch(url + 'Groups/Delete ', {
            method: 'POST',
            credentials: 'include',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(id)
          }).then(response => {
              return response;
          });
  }