﻿using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository;
using ClientSupportSystem.Repository.Interfaces;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Microsoft.ML.Transforms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace ClientSupportSystem.PredictionService
{
	public class PredictionService : IPredictionService
	{
		private IHelpRepostory _helpRepo;
		private ITicketRepository _ticketRepo;
		private IGroupsRepository _groupRepo;
		private static TransformerChain<KeyToValueMappingTransformer> _helpModel;
		private static TransformerChain<KeyToValueMappingTransformer> _ticketsModel;
		private static TransformerChain<KeyToValueMappingTransformer> _groupModel;
		private static List<int> _ticketSource;
		private static bool _isHelpDataLoaded = false;
		private static bool _isTicketDataLoaded = false;
		private static bool _isGroupDataLoaded = false;

		public PredictionService(IHelpRepostory helpRepo, ITicketRepository ticketRepo, IGroupsRepository groupRepo)
		{
			_helpRepo = helpRepo;
			_ticketRepo = ticketRepo;
			_groupRepo = groupRepo;
		}

		public PredictionModel GetHelp(string text)
		{
			if (!_isHelpDataLoaded)
			{
				return null;
			}

			var model = _helpModel;

			MLContext mlContext = new MLContext(seed: 0);

			var predFunction = model.CreatePredictionEngine<SentimentData, SentimentPrediction>(mlContext);

			var query = new SentimentData { Features = text };

			var prediction = predFunction.Predict(query);

			var score = prediction.Score.Max();

			var item = _helpRepo.GetById(prediction.Prediction);

			return new PredictionModel
			{
				Id = item.Id,
				Name = item.Name,
				PredictionScore = (int)(score * 100)
			};
		}

		public IEnumerable<PredictionModel> GetTasks(string text, int id)
		{
			if (!_isTicketDataLoaded)
			{
				return null;
			}

			MLContext mlContext = new MLContext(seed: 0);

			var model = _ticketsModel;

			var predFunction = model.CreatePredictionEngine<SentimentData, SentimentPrediction>(mlContext);

			var query = new SentimentData { Features = text };

			var prediction = predFunction.Predict(query);

			var score = prediction.Score.Max();

			List<PredictionModel> result = new List<PredictionModel>();

			var list = prediction.Score.Select((q, i) => new { Pos = i, Val = q })
				.OrderByDescending(q => q.Val)
				.Take(3)
				.ToList();

			foreach (var p in list)
			{
				//if (prediction.Score[i] >= 0.5)
				{
					var sourceId = _ticketSource[p.Pos];
					if (sourceId == id)   //пропускаем, если id совпадают
					{
						continue;
					}
					var item = _ticketRepo.GetTicket(sourceId);
					result.Add(new PredictionModel { Id = sourceId, Name = item.Title, PredictionScore = (int)(p.Val * 100) });
				}
			}

			return result;
		}

		public void TrainTicketsModel()
		{
			IEnumerable<TicketPredictionModel> data = _ticketRepo.GetAllPredictionTickets(1000);

			if (!data.Any() && data.Count() <= 2)
			{
				return;
			}

			//.SelectMany(q => q.Info.Split("."), (obj, Info) => new { obj.Id, Info })
			var source = data.Select(q => new { Label = q.Id, Features = q.Info }).ToList();

			//сохраняем, чтобы потом вытащить несколько предсказаний из модели
			_ticketSource = source.Select(q => q.Label).ToList();

			MLContext mlContext = new MLContext(seed: 0);

			IDataView dataView = mlContext.Data.LoadFromEnumerable(source);

			var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Label", "Label").Append(mlContext.Transforms.Text.FeaturizeText("Features", "Features"));

			///
			IEstimator<ITransformer> trainer = mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent(DefaultColumnNames.Label, DefaultColumnNames.Features);
			///OR
			//var averagedPerceptronBinaryTrainer = mlContext.BinaryClassification.Trainers.AveragedPerceptron("Label", "Features", numIterations: 10);
			//IEstimator<ITransformer> trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(averagedPerceptronBinaryTrainer);
			///

			var trainingPipeline = pipeline.Append(trainer).Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

			var model = trainingPipeline.Fit(dataView);

			_ticketsModel = model;

			_isTicketDataLoaded = true;
		}

		public void TrainHelpsModel()
		{
			IEnumerable<HelpModel> data = _helpRepo.GetAll();

			if (data == null || !data.Any() && data.Count() >= 2)
			{
				return;
			}

			var source = data.Select(q => new { Label = q.Id, Features = q.Info }).ToList();

			MLContext mlContext = new MLContext(seed: 0);

			IDataView dataView = mlContext.Data.LoadFromEnumerable(source);

			var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Label", "Label").Append(mlContext.Transforms.Text.FeaturizeText("Features", "Features"));

			///
			IEstimator<ITransformer> trainer = mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent(DefaultColumnNames.Label, DefaultColumnNames.Features);
			///OR
			//var averagedPerceptronBinaryTrainer = mlContext.BinaryClassification.Trainers.AveragedPerceptron("Label", "Features", numIterations: 10);
			//IEstimator<ITransformer> trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(averagedPerceptronBinaryTrainer);
			///

			var trainingPipeline = pipeline.Append(trainer).Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

			var model = trainingPipeline.Fit(dataView);

			_helpModel = model;

			_isHelpDataLoaded = true;
		}

		public void TrainGroupsModel()
		{

			IEnumerable<GroupItemModel> data = _groupRepo.GetAll()
				.Where(q => !String.IsNullOrEmpty(q.Keys))
				.ToList();

			if (data == null || !data.Any() && data.Count() >= 2)
			{
				return;
			}

			var source = data.Select(q => new { Label = q.Id, Features = q.Keys })
				.SelectMany(q => q.Features.Split(" "), (obj, Features) => new { obj.Label, Features })
				.ToList();

			MLContext mlContext = new MLContext(seed: 0);

			var trainingDataView = mlContext.Data.LoadFromEnumerable(source);

			//var dataProcessPipeline = mlContext.Transforms.Conversion.MapValueToKey(outputColumnName: "Label", inputColumnName: nameof(SentimentData.Label))
			//				.Append(mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(SentimentData.Features)))
			//				.Append(mlContext.Transforms.Concatenate(outputColumnName: "Features", inputColumnNames: "Name"))
			//				.AppendCacheCheckpoint(mlContext);

            //стохастическая классиф и ovnevsall
			var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Label", "Label").Append(mlContext.Transforms.Text.FeaturizeText("Features", "Features"));
			var averagedPerceptronBinaryTrainer = mlContext.BinaryClassification.Trainers.AveragedPerceptron("Label", "Features", numIterations: 10);
			IEstimator<ITransformer> trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(averagedPerceptronBinaryTrainer);

			var trainingPipeline = pipeline.Append(trainer)
				   .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

			var trainedModel = trainingPipeline.Fit(trainingDataView);

			_groupModel = trainedModel;

			_isGroupDataLoaded = true;
		}

		public PredictionModel GetGroup(string text)
		{
			if (!_isGroupDataLoaded || String.IsNullOrEmpty(text) || text.Contains("undefined"))
			{
				return null;
			}

			var model = _groupModel;

			MLContext mlContext = new MLContext(seed: 0);

			var predFunction = model.CreatePredictionEngine<SentimentData, SentimentPrediction>(mlContext);

			var query = new SentimentData { Features = text };

			var prediction = predFunction.Predict(query);

			var score = prediction.Score.Max();

			if (score <= 0.2)
			{
				return null;
			}

			var item = _groupRepo.GetById(prediction.Prediction);

			return new PredictionModel
			{
				Id = item.Id,
				Name = item.Name,
				PredictionScore = (int)(score * 100)
			};
		}
	}
}
