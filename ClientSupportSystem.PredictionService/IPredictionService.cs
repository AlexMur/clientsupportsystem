﻿using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSupportSystem.PredictionService
{
	public interface IPredictionService
	{
		void TrainTicketsModel();
		void TrainHelpsModel();
		void TrainGroupsModel();
		PredictionModel GetHelp(string text);
		PredictionModel GetGroup(string text);
		IEnumerable<PredictionModel> GetTasks(string text, int id);
	}
}
