﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSupportSystem.PredictionService
{
	public class SentimentData
	{
		//[Column(ordinal: "0", name: "Label")]
		public int Label;
		//[Column(ordinal: "1")]
		public string Features;
	}

	public class SentimentPrediction
	{
		[ColumnName("PredictedLabel")]
		public int Prediction { get; set; }

		//[ColumnName("Probability")]
		//public float Probability { get; set; }

		[ColumnName("Score")]
		public float[] Score { get; set; }
	}
}
