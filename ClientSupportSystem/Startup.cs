﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prediction = ClientSupportSystem.PredictionService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using ClientSupportSystem.Repository;
using ClientSupportSystem.Repository.Interfaces;
using ClientSupportSystem.Repository.Repositories;

namespace ClientSupportSystem
{
	public class Startup
	{
		public static IConfiguration Configuration { get; set; }

		public Startup(IConfiguration config)
		{
			Configuration = config;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			var connectionString = Configuration.GetConnectionString("DefaultConnection");

			services.AddCors(q => q.AddPolicy("MyPolicy", builder =>
			 {
				 builder.AllowAnyOrigin()
						.AllowAnyMethod()
						.AllowAnyHeader()
						.AllowCredentials();
			 }));
			services.AddMvc();
			services.AddDbContext<DbContext, ClientSupportContext>(
				options => options.UseSqlServer(connectionString)
			);

			services.AddTransient<ClientSupportContext>();
			//services.AddTransient(typeof(IDbRepository<>), typeof(DbRepository<>));
			services.AddTransient(typeof(IUsersRepository), typeof(UsersRepository));
			services.AddTransient(typeof(ITicketRepository), typeof(TicketRepository));
			services.AddTransient(typeof(IHelpRepostory), typeof(HelpRepository));
			services.AddTransient(typeof(Prediction.IPredictionService), typeof(Prediction.PredictionService));
			services.AddTransient(typeof(IHistoryRepository), typeof(HistoryRepository));
			services.AddTransient(typeof(IActionRepository), typeof(ActionRepository));
			services.AddTransient(typeof(IRoleRepository), typeof(RoleRepository));
			services.AddTransient(typeof(IGroupsRepository), typeof(GroupsRepository));

			var sp = services.BuildServiceProvider();
			var fooService = sp.GetService<Prediction.IPredictionService>();

			Task.Run(() =>
			{
				fooService.TrainHelpsModel();
				fooService.TrainTicketsModel();
				fooService.TrainGroupsModel();
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseCors("MyPolicy");
			app.UseMvcWithDefaultRoute();

			//app.Run(async (context) =>
			//{
			//    await context.Response.WriteAsync("Hello World!");
			//});


			//var serviceProvider = app.ApplicationServices;
			//var hostingEnv = serviceProvider.GetService<IHostingEnvironment>();

		}
	}
}
