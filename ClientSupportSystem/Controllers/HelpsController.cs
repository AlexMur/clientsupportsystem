﻿using ClientSupportSystem.Model.DbEntity;
using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientSupportSystem.Controllers
{
	public class HelpsController : Controller
	{
		private IHelpRepostory _repo;

		public HelpsController(IHelpRepostory repo)
		{
			_repo = repo;
		}

		[HttpGet]
		public IActionResult GetAll()
		{
			return Ok(_repo.GetAll());
		}

		[HttpGet]
		public IActionResult GetById(int id)
		{
			return Ok(_repo.GetById(id));
		}

		[HttpPost]
		public IActionResult Create([FromBody]HelpModel model)
		{
			_repo.Create(model);
			return Ok();
		}

		[HttpPost]
		public IActionResult Update([FromBody]HelpModel model)
		{
			var item = new Help();
			item.Id = model.Id;
			item.Info = model.Info;
			item.Name = model.Name;
			_repo.Update(item);
			return Ok();
		}

		[HttpPost]
		public IActionResult Delete([FromBody]int id)
		{
			_repo.Remove(id);
			return Ok();
		}
	}
}
