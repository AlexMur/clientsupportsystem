﻿using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientSupportSystem.Controllers
{
	public class GroupsController : Controller
	{
		private IGroupsRepository _repo;

		public GroupsController(IGroupsRepository repo)
		{
			_repo = repo;
		}

		[HttpGet]
		public IActionResult GetAll()
		{
			return Ok(_repo.GetAll());
		}

		[HttpGet]
		public IActionResult GetById(int id)
		{
			return Ok(_repo.GetById(id));
		}

		[HttpPost]
		public IActionResult Create([FromBody]GroupItemModel model)
		{
			_repo.Create(model);
			return Ok();
		}

		[HttpPost]
		public IActionResult Update([FromBody]GroupItemModel model)
		{
			_repo.Update(model);
			return Ok();
		}

		[HttpPost]
		public IActionResult Delete([FromBody]int id)
		{
			_repo.Delete(id);
			return Ok();
		}
	}
}
