﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prediction = ClientSupportSystem.PredictionService;
using Microsoft.AspNetCore.Mvc;
using ClientSupportSystem.Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClientSupportSystem.Controllers
{
	public class PredictionController : Controller
	{
		private readonly Prediction.IPredictionService _predictionService;

		public PredictionController(Prediction.IPredictionService service)
		{
			_predictionService = service;
		}
		/// <summary>
		/// Получить ссылку на справочник
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetHelpPrediction(string text)
		{
			var prediction = _predictionService.GetHelp(text);
			return Ok(prediction);
		}

		/// <summary>
		/// Получить ссылку на решенные задачи
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetTasksPrediction(string text, int id)
		{
			var prediction = _predictionService.GetTasks(text, id);
			return Ok(prediction);
		}

		[HttpGet]
		public IActionResult GetGroupPrediction(string text)
		{
			var prediction = _predictionService.GetGroup(text);
			return Ok(prediction);
		}
	}
}
