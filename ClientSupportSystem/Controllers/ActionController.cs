﻿using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientSupportSystem.Controllers
{
	public class ActionController : Controller
	{
		private IActionRepository _repo;

		public ActionController(IActionRepository repo)
		{
			_repo = repo;
		}

		[HttpGet]
		public ActionResult GetActions()
		{
			return Ok(_repo.GetAll());
		}
	}
}
