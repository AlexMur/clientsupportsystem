﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ClientSupportSystem.Controllers
{
	public class HomeController : Controller
	{
		private readonly ITicketRepository _repo;

		public HomeController(ITicketRepository repo)
		{
			_repo = repo;
		}

		public IActionResult Index()
		{
			var tickets = _repo.GetAllTickets();
			return Ok(tickets);
		}
	}
}