﻿using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientSupportSystem.Controllers
{
	public class RoleController : Controller
	{
		private IRoleRepository _repo;

		public RoleController(IRoleRepository repo){
			_repo = repo;
		}	

		[HttpGet]
		public IActionResult GetRoles()
		{
			return Ok(_repo.GetAll());
		}
	}
}
