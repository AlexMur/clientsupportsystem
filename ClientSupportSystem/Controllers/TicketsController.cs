﻿using System;
using System.Collections.Generic;
using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ClientSupportSystem.Controllers
{
	public class TicketsController : Controller
	{
		private ITicketRepository _repo;
		private IHistoryRepository _historyRepo;

		public TicketsController(ITicketRepository repo, IHistoryRepository historyRepo)
		{
			_repo = repo;
			_historyRepo = historyRepo;
		}

		[HttpGet]
		public IActionResult Index()
		{
			var tickets = _repo.GetAllTickets();
			return Ok(tickets);
		}

		[HttpGet]
		public IActionResult Get(int id)
		{
			var ticket = _repo.GetTicket(id);
			return Ok(ticket);
		}

		[HttpPost]
		public IActionResult Create([FromBody]TicketItemModel item)
		{
			_repo.CreateTicket(item);
			return Ok();
		}

		[HttpPost]
		public IActionResult Update([FromBody]TicketItemModel item)
		{
			_repo.UpdateTicket(item);
			return Ok();
		}

		[HttpPost]
		public IActionResult Delete([FromBody]int id)
		{
			_repo.DeleteTicket(id);
			return Ok();
		}

		[HttpPost]
		public IActionResult AddHistory([FromBody]HistoryArgs item)
		{
			var model = new HistoryItemModel();
			model.Info = item.Info;
			model.StaffId = item.StaffId;//temp
			model.TicketId = item.TicketId;

			_historyRepo.AddHistory(model);
			return Ok();
		}

		[HttpPost]
		public IActionResult RemoveHistory([FromBody]int id) {
			_historyRepo.DeleteHistory(id);
			return Ok();
		}

		[HttpPost]
		public IActionResult UpdateHistory([FromBody]HistoryArgs item)
		{
			var model = new HistoryItemModel();
			model.Info = item.Info;
			model.StaffId = item.StaffId;//temp
			model.Id = item.Id;
			_historyRepo.UpdateHistory(model);
			return Ok();
		}
	} 
}