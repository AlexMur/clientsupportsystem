﻿using ClientSupportSystem.Model.Models;
using ClientSupportSystem.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientSupportSystem.Controllers
{
	public class UsersController : Controller
	{
		private IUsersRepository _repo;

		public UsersController(IUsersRepository repo)
		{
			_repo = repo;
		}

		[HttpGet]
		public IActionResult GetUsers()
		{
			return Ok(_repo.GetUsers());
		}

		[HttpGet]
		public IActionResult GetUser(int id)
		{
			return Ok(_repo.GetUser(id));
		}

		[HttpGet]
		public IActionResult GetStaff()
		{
			return Ok(_repo.GetStaff());
		}

		[HttpGet]
		public IActionResult GetAllPersonal()
		{
			return Ok(_repo.GetAll());
		}

		[HttpPost]
		public IActionResult UpdateUser([FromBody]UserModel model)
		{
			_repo.UpdateUser(model);
			return Ok();
		}

		[HttpPost]
		public IActionResult CreateUser([FromBody]UserModel model)
		{
			_repo.SaveUser(model);
			return Ok();
		}

		[HttpGet]
		public IActionResult GetCurrentUser()
		{
			return Ok(_repo.GetAdmin());
		}
	}
}
