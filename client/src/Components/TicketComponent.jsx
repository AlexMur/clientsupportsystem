import React from 'react';
import {
    getStaff, 
    getUsers, 
    getTicket, 
    saveTicket, 
    updateTicket, 
    addHistory, 
    deleteHistory, 
    updateHistory, 
    getHelpPrediction,
    getTaskPrediction,
    getActions,
    deleteTicket,
    getAllGroups,
    getGroupPrediction
} from '../ApiService.js'
import { Link } from "react-router-dom";

class TicketComponent extends React.Component{
    constructor(props){
        super(props);
        let id = this.props.match.params.id;
        this.state = {
            id: id,
            ticket: {
                title: "",
                info: "",
                staffId: null,
                userId: null,
                histories: [],
                userId: 1,
                staffId: 1,
                actionTypeId: 1,
                groupId: ''
            },
            newHistoryInfo: "",
            editHistoryInfo: { info: ''},
            users: [],
            staff: [],
            actions: [],
            groups: [],
            helpPrediction: null,
            tasksPrediction: [],
            groupPrediction: null
        };

        this.saveTicket = this.saveTicket.bind(this);
        this.cancel = this.cancel.bind(this);
        this.addNewHistory = this.addNewHistory.bind(this);
        this.updateHistory = this.updateHistory.bind(this);
        this.changeHistoryItem = this.changeHistoryItem.bind(this);
        this.getHelp = this.getHelp.bind(this);
        this.getTaskPrediction = this.getTaskPrediction.bind(this);
        this.getGroupPrediction = this.getGroupPrediction.bind(this);
        this.delete = this.delete.bind(this);
    }

    async getHelp(txt){
        let res = await getHelpPrediction(txt);
        return res;
    }

    async getTaskPrediction(txt, id){
        let res = await getTaskPrediction(txt, id);
		return res;
    }

    async getGroupPrediction(txt){
        if(txt === null || txt === undefined){
            return;
        }

        if(!this.state.id)
        {
            let res = await getGroupPrediction(txt);
            this.setState({ groupPrediction: res});
        }
    }

    async componentDidMount(){
        let users = await getUsers();
        let staff = await getStaff();
        let actions = await getActions();
        let groups = await getAllGroups();

        let ticket = this.state.ticket;
        ticket.staffId = staff[0].id;
        ticket.userId = users[0].id;
        ticket.actionTypeId = actions[0].id;
        ticket.groupId = '';

        if(this.state.id){
            ticket = await getTicket(this.state.id);
            let help = this.getHelp(ticket.info);
            let tasks = this.getTaskPrediction(ticket.info, this.state.id);
            Promise.all([help, tasks]).then((values)=> {
                const [ helpPrediction, tasksPrediction ] = values;
                this.setState({ helpPrediction, tasksPrediction});
            });
            this.setState({ ticket});
        }
        this.setState({ users, staff, ticket, actions, groups});
    }

    async delete(){
        await deleteTicket(this.state.id);
        this.props.history.push('/tickets');
    }

    changeHistoryItem(event){
        var editHistoryInfo = this.state.editHistoryInfo;
        editHistoryInfo.info = event.target.value;
        this.setState({editHistoryInfo});
    }

    async saveTicket(event) {
        event.stopPropagation();
        event.preventDefault();

        let res;
        if(this.state.id){
            res = await updateTicket(this.state.ticket);
        } else {
            res = await saveTicket(this.state.ticket);
        }
        if(res.ok){
            this.props.history.push('/tickets');
        }
    }

    cancel(event){
        event.stopPropagation();
        event.preventDefault();
        this.props.history.push('/tickets');
    }

    async updateHistory(event){
        event.stopPropagation();
        event.preventDefault();

        let info = this.state.editHistoryInfo;
        info.staffId =  this.state.ticket.staffId;

        await updateHistory(info);
        
        let ticket = await getTicket(this.state.id);
        
        let editHistoryInfo = this.state.editHistoryInfo;
        editHistoryInfo = {info: ''};

        this.setState({ticket, editHistoryInfo});
    }

    async deleteHistoryItem(id){
        await deleteHistory(id);
      
        let ticket = await getTicket(this.state.id);
        
        let editHistoryInfo = this.state.editHistoryInfo;
        editHistoryInfo = {info: ''};
        
        this.setState({ticket, editHistoryInfo});
    }

    async addNewHistory(event){
        event.stopPropagation();
        event.preventDefault();

        let res = await addHistory({ 
            info: this.state.newHistoryInfo,
            staffId: this.state.ticket.staffId,
            ticketId: this.state.id
        });
        
        if(res.ok){
            let ticket = await getTicket(this.state.id);
        
            let newHistoryInfo = this.state.newHistoryInfo;
            newHistoryInfo = "";
            
            this.setState({ticket, newHistoryInfo});
        }
    }

    render(){
        const users = this.state.users.map((q,i)=>
            <option key={i} value={q.id}>{q.name}</option>
        );

        const staff = this.state.staff.map((q,i)=>
            <option key={i} value={q.id}>{q.name}</option>
        );

        const actions = this.state.actions.map((q,i)=>
            <option key={i} value={q.id}>{q.name}</option>
        );

        const groups = this.state.groups.map((q,i)=>
        <option key={i} value={q.id}>{q.name}</option>
    );

        const history = this.state.ticket.histories && this.state.ticket.histories.map((q,i)=>
            <div key={i} className="history-item">
                <div className={this.state.editHistoryInfo && this.state.editHistoryInfo.id === q.id ? 'hidden' : ''}>
                    <div>{new Date(q.date).toLocaleString()}</div>
                    <div>{q.staffName}</div>
                    <div>{q.info}</div>
                    <div>
                        <a href="#" onClick={()=>this.deleteHistoryItem(q.id)}>Удалить</a>
                        <a href="#" onClick={()=>{
                            let copy = JSON.parse(JSON.stringify(q));
                            let editHistoryInfo = this.state.editHistoryInfo;
                            editHistoryInfo = copy;
                            this.setState({editHistoryInfo});
                        }}>Редактировать </a>
                    </div>
                </div>
                <div className={this.state.editHistoryInfo && this.state.editHistoryInfo.id == q.id ? '' : 'hidden'}>
                    <div>
                        <input onChange={this.changeHistoryItem} value={this.state.editHistoryInfo.info}></input>
                    </div>
                    <div>
                        <a href="#" onClick={this.updateHistory}>Сохранить</a>
                        <a href="#" onClick={()=>{
                            let editHistoryInfo = this.state.editHistoryInfo;
                            editHistoryInfo = {info: ''};
                            this.setState({editHistoryInfo});
                        }}>Отмена</a>
                    </div>
                </div>
            </div>
        );

        const ticketsPredictions = this.state.tasksPrediction.map((q, i)=>
           <a target="_blank" key={i} href={'/ticket/' + q.id}>{q.name}</a>
        //    + '(' + q.predictionScore  +'%)'
        );

        return <div className="table">
            <div>
                 <a href="#" onClick={this.saveTicket}>Сохранить</a>
                 <a href="#" onClick={this.cancel}>Отмена</a>
                 {this.state.id && <a href="#" onClick={this.delete}>Удалить</a>}
            </div>
            <div>
                <div className="left-column">
                    <label>Название:</label>
                </div>
                <div className="right-column">
                    <input type="text" value={this.state.ticket.title} onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.title = event.target.value;
                        this.setState({ticket});
                    }}/>
                </div>
            </div>
            <div>
                <div className="left-column">
                    <label>Описание: </label>
                </div>
                <div className="right-column">
                    <textarea value={this.state.ticket.info} onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.info = event.target.value;
                        this.setState({ticket});
                    }} onBlur={()=>this.getGroupPrediction(this.state.ticket.info)}/>
               </div>
            </div>
            <div>
                <div className="left-column">
                    <label>Пользователь:</label>
                </div>
                <div className="right-column">
                    <select value={this.state.ticket.userId} onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.userId = event.target.value;
                        this.setState({ticket});
                    }}>{users}</select>
                </div>
            </div>
            <div>
                <div className="left-column">
                    <label>Сотрудник:</label>
                </div>
                <div className="right-column">
                    <select value={this.state.ticket.staffId}  onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.staffId = event.target.value;
                        this.setState({ticket});
                    }}>
                        {staff}
                    </select>
                </div>
            </div>
            <div>
                <div className="left-column">
                    <label>Состояние:</label>
                </div>
                <div className="right-column">
                    <select value={this.state.ticket.actionTypeId}  onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.actionTypeId = event.target.value;
                        this.setState({ticket});
                    }}>
                        {actions}
                    </select>
                </div>
            </div>
            <div>
                <div className="left-column">
                    <label>Группа:</label>
                </div>
                <div className="right-column">
                    <select value={this.state.ticket.groupId || ''} onChange={(event)=> {
                        let ticket = this.state.ticket;
                        ticket.groupId = event.target.value;
                        this.setState({ticket});
                    }}>
                        <option key='-1' value=''></option>
                        {groups}
                    </select>
                </div>
            </div>
            <div>
                <hr></hr>
                {history}
                <hr></hr>
                {this.state.id && <div className="add-history-item">
                    <input value={this.state.newHistoryInfo} onChange={(event)=>{
                        let newHistoryInfo = this.state.newHistoryInfo;
                        newHistoryInfo = event.target.value;
                        this.setState({newHistoryInfo})
                    }}/>
                    <button href="#" onClick={this.addNewHistory}>Добавить</button>
                 </div>
                }
            </div>
            {
                this.state.helpPrediction !== null &&
                this.state.helpPrediction.id && <div className="prediction-helps">
                    Справка:
                    <a target="_blank" href={'/help/view/' + this.state.helpPrediction.id}>{this.state.helpPrediction.name}</a> 
                    {/* + '(' + this.state.helpPrediction.predictionScore  +'%)' */}
                </div>
            }
            {
                this.state.tasksPrediction[0] && <div className="prediction-tickets">
                    Похожие тикеты:
                    {ticketsPredictions}
                </div>
            }
            {
                 this.state.groupPrediction !== null &&
                 this.state.groupPrediction.id && <div className="prediction-group">
                    Возможная группа: <a href="#" onClick={(event)=>{
                        event.preventDefault();
                        event.stopPropagation();

                        let ticket = this.state.ticket;
                        ticket.groupId = this.state.groupPrediction.id;
                        this.setState({ticket});
                    }}>{this.state.groupPrediction.name}</a>
                </div>
            }
        </div>;
    }
}

export default TicketComponent;