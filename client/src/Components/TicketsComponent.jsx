import React from 'react';
import { getAllTickets} from '../ApiService.js'
import { Link } from "react-router-dom";

class TicketsComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tickets: []
        };

        this.getShortName = this.getShortName.bind(this);
    }

    async componentDidMount(){
        let tickets =  await getAllTickets();
        this.setState({
            tickets
        });
    }

    getShortName(name){
        var words = name.split(" ");
        var result = words[0];
        for(var i = 1; i < words.length; i ++){
            result += " " + words[i][0] + ".";
        }
        return result;
    }

    render(){
        const rows = this.state.tickets.map((q,i)=>
            <tr key={i}>
                <td>{q.title}</td>
                <td>{this.getShortName(q.userName)}</td>
                <td>{q.staffName}</td>
                <td>{q.groupName}</td>
                <td>{new Date(q.createDate).toLocaleString()}</td>
                <td>{q.actionTypeName}</td>
                <td>
                    <Link to={'/ticket/' + q.id}>Редактировать</Link>
                </td>
            </tr>
        );

        return <div className="main-content">
            <div>
                <Link to={'/ticket/'}>Создать тикет</Link>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Название</th>
                        <th>Пользователь</th>
                        <th>Сотрудник</th>
                        <th>Группа</th>
                        <th>Дата обращения</th>
                        <th>Состояние</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        </div>;
    }
}

export default TicketsComponent;