import React from 'react';
import { Link } from "react-router-dom";

class MenuComponent extends React.Component{
    constructor(){
        super();
        this.state = {
            menu: [
                {
                    path: '/tickets',
                    name: 'Заявки'
                },
                {
                    path: '/helps',
                    name: 'Справочники'
                },
                {
                    path: '/users',
                    name: 'Сотрудники'
                },
                {
                    path: '/groups',
                    name: 'Группы'
                }
            ]
        };
    }

    render(){
        const menu = this.state.menu.map((q, i)=>
            <Link to={q.path} key={i}>{q.name}</Link>
        );

        return <nav>
            {menu}
        </nav>;
    }
}

export default MenuComponent;