import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MenuComponent from './MenuComponent';
import TicketComponent from './Components/TicketComponent';
import TicketsComponent from './Components/TicketsComponent';
import UsersComponent from './Components/UsersComponent';
import UserComponent from './Components/UserComponent';
import HelpsComponent from './Components/HelpsComponent';
import HelpComponent from './Components/HelpComponent';
import HelpViewComponent from './Components/HelpViewComponent';
import GroupsComponent from './Components/GroupsComponent';
import GroupComponent from './Components/GroupComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Router>
            <MenuComponent></MenuComponent>
            <Switch>
                <Route exact path="/" component={TicketsComponent} />
                <Route exact path="/tickets" component={TicketsComponent} />
                <Route path="/ticket/:id?" component={TicketComponent} />
                <Route exact path="/users" component={UsersComponent} />
                <Route path="/user/:id?" component={UserComponent} />
                <Route exact path="/helps" component={HelpsComponent} />
                <Route path="/help/view/:id" component={HelpViewComponent} />
                <Route path="/help/:id?" component={HelpComponent} />
                <Route exact path="/groups" component={GroupsComponent} />
                <Route path="/group/:id?" component={GroupComponent} />
            </Switch>
          </Router>
      </div>
    );
  }
}

export default App;