﻿CREATE TABLE [dbo].[Groups]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	[Name] NVARCHAR(200) NOT NULL, 
    [KeyWords] NVARCHAR(500) NOT NULL
)
