﻿Delete from Groups;

DBCC CHECKIDENT ('Groups', RESEED, 1);

SET IDENTITY_INSERT Groups ON

INSERT INTO Groups([Id],[Name], [KeyWords]) VALUES(1, N'Сеть', N'Сеть');
INSERT INTO Groups([Id],[Name], [KeyWords]) VALUES(2, N'Моб. устройства', N'Телефон смартфон планшет тачпад');
INSERT INTO Groups([Id],[Name], [KeyWords]) VALUES(3, N'Моб. ноутбуки', N'Ноутбук laptop');

SET IDENTITY_INSERT Groups OFF
