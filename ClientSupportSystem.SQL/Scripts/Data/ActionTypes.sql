﻿Delete from ActionTypes;

DBCC CHECKIDENT ('ActionTypes', RESEED, 1);

SET IDENTITY_INSERT ActionTypes ON

INSERT INTO ActionTypes([Id],[Name]) VALUES(1, N'Новый');
INSERT INTO ActionTypes([Id],[Name]) VALUES(2, N'В работе');
INSERT INTO ActionTypes([Id],[Name]) VALUES(3, N'Решено');
INSERT INTO ActionTypes([Id],[Name]) VALUES(4, N'Отменено');
INSERT INTO ActionTypes([Id],[Name]) VALUES(5, N'Остановлено');

SET IDENTITY_INSERT ActionTypes OFF
