﻿Delete from UserRoles;

DBCC CHECKIDENT ('UserRoles', RESEED, 1);

SET IDENTITY_INSERT UserRoles ON

INSERT INTO UserRoles([Id], [Name]) VALUES(1, N'Пользователь');
INSERT INTO UserRoles([Id], [Name]) VALUES(2, N'Сотрудник');

SET IDENTITY_INSERT UserRoles OFF
