﻿Delete from Users;

DBCC CHECKIDENT ('Users', RESEED, 1);

SET IDENTITY_INSERT Users ON

INSERT INTO Users([Id], [Name], [RoleId]) VALUES(1, N'Иванов Иван Иванович', 1);
INSERT INTO Users([Id], [Name], [RoleId]) VALUES(2, N'Петров Петр Петрович', 1);
INSERT INTO Users([Id], [Name], [RoleId]) VALUES(3, N'Сергеев Сергей Сергеевич', 1);
INSERT INTO Users([Id], [Name], [RoleId]) VALUES(4, N'Сотрудник1', 2);
INSERT INTO Users([Id], [Name], [RoleId]) VALUES(5, N'Сотрудник2', 2);
INSERT INTO Users([Id], [Name], [RoleId]) VALUES(6, N'Администратор', 2);

SET IDENTITY_INSERT Users OFF