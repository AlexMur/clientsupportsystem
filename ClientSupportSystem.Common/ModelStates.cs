﻿using System;

namespace ClientSupportSystem.Common
{
	public enum ModelStates {
		NonTrained = 1,
		InTraining,
		Trained
	}
}
